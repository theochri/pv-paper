# Import external libraries
import pandas as pd

import epw
import pv_helper_fun as fct


#create weather DF
filename = 'https://energyplus.net/weather-download/north_and_central_america_wmo_region_4/USA/WA/USA_WA_Seattle-Tacoma.Intl.AP.727930_TMY3/USA_WA_Seattle-Tacoma.Intl.AP.727930_TMY3.epw'
coerce_year = 2001
weather, meta = epw.read_epw(filename, coerce_year)

options = {'azi': [90, 90, 90, 90, 90, 90, 90, 180, 180, 180, 180, 180, 180, 180, 270, 270, 270, 270, 270, 270, 270],
           'tilt': [10, 10, 30, 30, 50, 50, 89, 10, 10, 30, 30, 50, 50, 89, 10, 10, 30, 30, 50, 50, 89], 
           'gcr': [0.95, 0.6, 0.85, 0.5, 0.8, 0.4, 0.9, 0.95, 0.6, 0.85, 0.5, 0.8, 0.4, 0.9, 0.95, 0.6, 0.85, 0.5, 0.8, 0.4, 0.9]}
options_df = pd.DataFrame(data=options)

#test = options_df.ix[1,'azi']
#test

results = pd.DataFrame(index=weather.index)

for count in range(0, 20):
# configure PV system
    PV_def, array_simple = fct.PV_config(options_df.loc[count,:]) # azi, tilt, gcr

#timeseries calculation
    tot_e_poa = fct.timeseries(weather, meta, PV_def)
    results = pd.concat([results, tot_e_poa], axis=1)
results.to_csv('results_190403.csv')