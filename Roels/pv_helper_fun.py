from pvfactors.pvarray import Array
import pvlib
import pandas as pd
from pvfactors.timeseries import calculate_radiosities_serially_perez
from pvfactors.timeseries import get_average_pvrow_outputs


def PV_config(orientation):
    # Set up the configuration
    PV_def = {
    'n_pvrows': 2,
    'pvrow_height': 2.,
    'pvrow_width': 1.,
    'surface_azimuth': orientation['azi'],
    'surface_tilt': orientation['tilt'],
    'gcr': orientation['gcr'],
    'solar_zenith': 60.,
    'solar_azimuth': 180.,  # sun located south
    'rho_ground': 0.2,
    'rho_front_pvrow': 0.01,
    'rho_back_pvrow': 0.01
    }
    # Create array object
    array_simple = Array(**PV_def)
    return PV_def, array_simple


def timeseries(weather, meta, PV_def):
    timestamps = weather.index
    solar_pos = pvlib.solarposition.get_solarposition(time=timestamps, \
                                                      latitude=meta['latitude'],\
                                                      longitude=meta['longitude'],\
                                                      altitude=meta['altitude'], \
                                                      pressure=weather['atmospheric_pressure'],\
                                                      method='nrel_numpy',\
                                                      temperature=weather['temp_air'])
    
    sol_zen = solar_pos['zenith']
    sol_azi = solar_pos['azimuth']
    surf_tilt = pd.Series(PV_def['surface_tilt']).repeat(8760)
    surf_tilt.index = weather.index
    surf_azi = pd.Series(PV_def['surface_azimuth']).repeat(8760) 
    surf_azi.index = weather.index
    
    df_registries, _ = calculate_radiosities_serially_perez((
        PV_def, timestamps,
        sol_zen, sol_azi, 
        surf_tilt, surf_azi, weather['dni'], weather['dhi']))
    
    df_avg_outputs = get_average_pvrow_outputs(df_registries)
    
    if PV_def['surface_tilt'] == 89:
        tot_e_poa = df_avg_outputs[0,'front','qinc']
    else:
        tot_e_poa = df_avg_outputs[1,'front','qinc']
#    tot_e_poa = pd.concat([row0, row1], axis=1)
#    tot_e_poa(str(options_df.loc[0,'azi']) + str(options_df.loc[0,'tilt']) + str(options_df.loc[0,'gcr']) + '.csv')
    return tot_e_poa